package com.example.android.basketballscoring;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static android.R.attr.id;

public class MainActivity extends AppCompatActivity {

    int team1Pts = 0;
    int team2Pts = 0;
    String team1Name = "";
    String team2Name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        team1Name = "Team A";
        team2Name = "Team B";
        DisplayTeam1Name(team1Name);
        DisplayTeam2Name(team2Name);

    }


    // team 1 stuff
    public void team1_3pts(View view) //button pressed
    {
        team1Pts += 3;
        DisplayTeam1Pts();
    }

    public void team1_2pts(View view) //button pressed
    {
        team1Pts += 2;
        DisplayTeam1Pts();
    }

    public void team1_freeThrow(View view) //button pressed
    {
        team1Pts++;
        DisplayTeam1Pts();
    }

    public void DisplayTeam1Name(String str)
    {
        TextView team1NameTextView = (TextView) findViewById(R.id.team1_name);
        team1NameTextView.setText("" + str);
    }

    public void DisplayTeam1Pts()
    {
        TextView team1PtsTextView = (TextView) findViewById(R.id.team1_score);
        team1PtsTextView.setText("" + team1Pts);
    }


    // team 2 stuff
    public void team2_3pts(View view) //button pressed
    {
        team2Pts += 3;
        DisplayTeam2Pts();
    }

    public void team2_2pts(View view) //button pressed
    {
        team2Pts += 2;
        DisplayTeam2Pts();
    }

    public void team2_freeThrow(View view) //button pressed
    {
        team2Pts++;
        DisplayTeam2Pts();
    }

    public void DisplayTeam2Name(String str)
    {
        TextView team2NameTextView = (TextView) findViewById(R.id.team2_name);
        team2NameTextView.setText("" + str);
    }

    public void DisplayTeam2Pts()
    {
        TextView team2PtsTextView = (TextView) findViewById(R.id.team2_score);
        team2PtsTextView.setText("" + team2Pts);
    }



    public void ResetStats(View view) //button pressed
    {
        team1Pts=0;
        team2Pts=0;
        DisplayTeam1Pts();
        DisplayTeam2Pts();
    }



}
